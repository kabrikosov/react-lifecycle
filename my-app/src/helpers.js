export const currying = (operation, waiting, current, evaluate) => {
    if (!waiting) {
        return [0, (number) => operation(+current, +number)];
    } else {
        return [evaluate(current), (arg) => operation(evaluate(current), +arg)];
    }
}