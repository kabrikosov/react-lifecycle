import React, {useState} from 'react';
import {currying} from "../helpers";
import styles from './Calculator.module.css';
import CalculatorButton from "./CalculatorButton";
import CalculatorOperationButton from "./CalculatorOperationButton";

const Calculator = () => {
    const [current, setCurrent] = useState(0);
    const [showing, setShowing] = useState(0);
    const [waiting, setWaiting] = useState(false);
    const [evaluate, setEvaluate] = useState(() => (arg) => 0);
    // Оптимизировал state, промежуточный результат сохраняется в callback'ах, поэтому saved не нужен.

    const handleNumberButtonClick = number => {
        if (current < 0) number *= -1;
        setCurrent(current => current * 10 + number);
        setShowing(current => current * 10 + number);
    }

    const operation = (operation) => {
        return () => {
            setCurrent(current => 0);
            setWaiting(waiting => true);
            let [nShowing, nEvaluate] = currying(operation, waiting, current, evaluate);
            setShowing(showing => nShowing);
            setEvaluate(evaluate => nEvaluate);
        }
    }

    const handleEraseButtonClick = () => {
        let val = current / 10 | 0
        setCurrent(current => val);
        setWaiting(waiting => true);
        setShowing(showing => val);
    }

    const handleEqualsButtonClick = () => {
        let val = evaluate(current);
        setCurrent(current => val);
        setWaiting(waiting => false);
        setShowing(showing => val);
    }

    const handleResetButtonClick = () => {
        setCurrent(current => 0);
        setShowing(showing => 0);
        setWaiting(waiting => false);
        setEvaluate(evaluate => arg => 0);
    }

    return (
        <div className={styles.calculator}>
            <div className={styles.calculatorDisplay}>
                {showing}
            </div>
            <div className={styles.calculatorButtons}>
                <div className={styles.calculatorNumbers}>
                    {
                        Array.of(7, 8, 9, 4, 5, 6, 1, 2, 3, 0).map(number =>
                            <CalculatorButton key={number} text={number} onClick={handleNumberButtonClick}/>
                        )
                    }
                </div>
                <div className={styles.calculatorOperations}>
                    {
                        Array.of(
                            ['erase', handleEraseButtonClick], // специфичная унарная операция, поэтому, не смог переиспользовать метод operation
                            ['divide', operation((a, b) => a / b)],
                            ['multiply', operation((a, b) => a * b)],
                            ['minus', operation((a, b) => a - b)],
                            ['plus', operation((a, b) => a + b)],
                        ).map(([name, f]) =>
                            <CalculatorOperationButton key={name} name={name} icon={`${name}.svg`} onClick={f}/>
                        )
                    }
                </div>
            </div>
            <CalculatorButton text={'='} onClick={handleEqualsButtonClick}/>
            <CalculatorButton text={'RESET'} onClick={handleResetButtonClick}/>
        </div>
    );
};

// У калькулятора нет пропов

export default Calculator;