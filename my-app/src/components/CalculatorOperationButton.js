import React from 'react';
import PropTypes from 'prop-types';
import styles from './CalculatorButton.module.css'

const CalculatorOperationButton = ({onClick, icon, name}) => {
    const handleClick = (e) => {
        e.preventDefault();
        onClick();
    }

    return (
        <button className={styles.customButton} onClick={handleClick} type={'button'}>
            <img className={styles.innerImage} src={icon} alt={name}/>
        </button>
    );
};

CalculatorOperationButton.propTypes = {
    name: PropTypes.string,
    onClick: PropTypes.func,
    icon: PropTypes.string
};

export default CalculatorOperationButton;