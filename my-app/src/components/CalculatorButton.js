import React from 'react';
import PropTypes from 'prop-types';
import styles from './CalculatorButton.module.css';

const CalculatorButton = ({text, onClick}) => {
    const handleClick = (e) => {
        e.preventDefault();
        onClick(text)
    }

    return (
        <button className={styles.customButton} onClick={handleClick} type={'button'}>
            {text}
        </button>
    );
};

CalculatorButton.propTypes = {
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onClick: PropTypes.func
};

export default CalculatorButton;